#include <iostream>
#include <string>
#include <fstream>
#include <map>
using namespace std;

main(int argc, char** argv){
	string thresh;
	string file_name;

	for(int i = 0; i < sizeof(argv[1])/sizeof(argv[1][0]); i++){
		file_name += argv[1][i];
	}

	int threshi;
	unsigned char info[54];

	FILE* File = fopen(argv[1], "rb");

	fread(info, sizeof(unsigned char), 54, File); 

	int width = *(int*)&info[18];
	int height = *(int*)&info[22];
	int size = 3 * width * height;
		
	unsigned char* data = new unsigned char[size];
	fread(data, sizeof(unsigned char), size, File); 
	fclose(File);

	cout << "Threshhold value for solarization effect[0-255]: ";
	getline(cin, thresh);
	cout << endl;
	cout << "number of pixels in " << file_name << "bmp: " << size/3 << endl;
	threshi = stoi(thresh);
		
	cout << "operation may take a few seconds pls wait" << endl;

	map<int, int> hist1;
	map<int, int> hist2;
	for(int i = 0; i < 256*3; i++){
		hist1[i] = 0;
		hist2[i] = 0;
	}
	

	for(int i = 0; i < size; i++){
		//cout << (data[i]+data[i+1]+data[i+2])/3;
		hist1[data[i]+256*(i%3)] += 1;
		if(data[i] > threshi){
			data[i] = 255 - data[i];
		}
		hist2[data[i]+256*(i%3)] += 1;
	}

	ofstream f(file_name + "bmp.txt");
	
	f << "number of pixels in " << file_name << "bmp: " << size/3 << "\n";

	for(int i = 0; i < 256*3; i++){
		if(i==0){
			cout << "histogram data for B: " << endl;
			f << "histstogram data for B: \n";
		}
		if(i==256){
                	cout << "histogram data for G: " << endl;
			f << "histogram data for G: \n";
		}
		if(i==512){
			cout << "histogram data for R: " << endl;
			f << "histogram data for R: \n";
	        }
	
		cout << i%256 << ": " << hist1[i] << " -> " << hist2[i] << endl;
		f << i%256 << ": " << hist1[i] << " -> " << hist2[i] << "\n";
	}
	f.close();
	cout << "histogram data has been saved to " << file_name << "bmp.txt" << endl;
	return 0;
}
